
@extends('layout.master')

@section('path')
    Cast
@endsection
@section('title')
Cast Add
@endsection 

@section('content')
<div>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Cast</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Usia cast</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Usia">
               
            </div>
            <div class="form-group">
                <label for="bio">Bio cast</label>
                <textarea class="form-control" name="bio" id="bio" placeholder="Masukkan Bio"> </textarea>
               
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection
