
@extends('layout.master')

@section('path')
    Cast
@endsection
@section('title')
Show Cast {{$cast->id}}
@endsection   

@section('content')
        <h4>{{$cast->nama}}</h4>
        <p>{{$cast->umur}}</p>
        <p>{{$cast->bio}}</p>
@endsection
